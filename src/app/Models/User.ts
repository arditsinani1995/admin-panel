interface User {
    name?:string,
    surname?:string,
    company?:string,
    jobTitle?:string,
    mobilePhone?:number,
    email?:string
}