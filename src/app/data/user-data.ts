export let Users:User[] = [{
    name: 'Ardit',
    surname: 'Sinani',
    company: 'Paperclicks',
    jobTitle: 'Software Engineer',
    mobilePhone: 695872394,
    email: 'arditsinani04@gmail.com'
},{
    name: 'John',
    surname: 'Doe',
    company: 'Rubik',
    jobTitle: 'Angular Developer',
    mobilePhone: 612345678 ,
    email: 'johndoe@gmail.com'
}]