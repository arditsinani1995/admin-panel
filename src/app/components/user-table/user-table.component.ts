import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Users } from './../../data/user-data'
import { MatTableDataSource, MatDialog, MatSnackBar } from '@angular/material';
import { UserFormComponent } from '../user-form/user-form.component';
import { DeleteModalComponent } from '../delete-modal/delete-modal.component';

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.css']
})
export class UserTableComponent implements OnInit {

  users:User[] = Users
  user:User = {
    name: '',
    surname: '',
    company:'',
    jobTitle:'',
    mobilePhone:0,
    email: ''
  }
  index: Number = 0

  displayedColumns: string[] = ['name', 'surname', 'company', 'jobTitle','mobilePhone','email','actions'];
  dataSource = new MatTableDataSource<User>(this.users);


  constructor(public dialog: MatDialog,
    private snackBar: MatSnackBar){

  }

  ngOnInit() {
  }

  addUser(){
    const dialogRef = this.dialog.open(UserFormComponent, {
      width: '50%',
      data: {user:this.user,mode:'add'}
    });
  
    dialogRef.componentInstance.onAdd.subscribe(result =>{
      this.users.push(result.user)
      this.dataSource = new MatTableDataSource<User>(this.users);
      this.user = {
        name: '',
        surname: '',
        company:'',
        jobTitle:'',
        mobilePhone:0,
        email: ''
      }
    })

  }

  showUserDetails(user,index): void {
    this.index = index
    const dialogRef = this.dialog.open(UserFormComponent, {
      width: '50%',
      data: {user,mode:'edit'}
    });

    if(index === 0){
      dialogRef.componentInstance.hasPrevious = false      
    }

    if (index === this.users.length - 1){
      dialogRef.componentInstance.hasNext = false
    }

    dialogRef.componentInstance.onEdit.subscribe(result =>{
      this.users[result.index] = result.user
      this.dataSource = new MatTableDataSource<User>(this.users);
    })

    dialogRef.componentInstance.onPrevious.subscribe(result=>{
      index--
      dialogRef.componentInstance.data.user = this.users[index];
      if(index === 0){
        dialogRef.componentInstance.hasPrevious = false
      } 
      if(index < this.users.length){
        dialogRef.componentInstance.hasNext = true
      } else {
        dialogRef.componentInstance.hasNext = false
      }
    })
    dialogRef.componentInstance.onNext.subscribe(result=>{
      index++
      dialogRef.componentInstance.data.user = this.users[index];
      if (index === this.users.length - 1){
        dialogRef.componentInstance.hasNext = false
      }
      if(index > 0){
        dialogRef.componentInstance.hasPrevious = true
      } else {
        dialogRef.componentInstance.hasPrevious = false
      }
    })
    this.index = 0
  }

  deleteUser(user,index){
    this.index = index
    const dialogRef = this.dialog.open(DeleteModalComponent, {
      width: '250px',
      data: {user, index}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.users.splice(result.index,1);
        this.dataSource = new MatTableDataSource<User>(this.users);
      }
    });
    this.index = 0
  }
}
