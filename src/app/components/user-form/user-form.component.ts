import { Component, Inject, EventEmitter } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent {

  onAdd = new EventEmitter();
  onEdit = new EventEmitter();
  onPrevious = new EventEmitter();
  onNext = new EventEmitter();
  hasPrevious: Boolean = true;
  hasNext: Boolean = true;
  
  
  constructor(public dialogRef: MatDialogRef<UserFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private snackBar: MatSnackBar) {
     
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  addUser(){
    if(!this.validateBeforeSibmit()){
      this.snackBar.open('Validate the form')
    } else {
      this.onAdd.emit(this.data)
      this.dialogRef.close()
    }
  }

  editUser(){
    if(!this.validateBeforeSibmit()){
      this.snackBar.open('Validate the form')
    } else {
      this.onEdit.emit(this.data)
      this.dialogRef.close()
    }
  }

  validateBeforeSibmit(){
    if(this.data.user.name === '' || this.data.user.surname === '' || this.data.user.company === ''
    || this.data.user.jobTitle === '' || this.data.user.mobilePhone === 0 || this.data.user.mobilePhone === null || this.data.user.email === ''){
      return false
    } else {
      return true
    }
  }

  getPrevious(){
    this.onPrevious.emit()
  }

  getNext(){
    this.onNext.emit()
  }

}
